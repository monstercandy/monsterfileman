FROM monstercommon

ADD lib /opt/MonsterFileman/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterFileman/lib/bin/fileman-install.sh && /opt/MonsterFileman/lib/bin/fileman-test.sh

ENTRYPOINT ["/opt/MonsterFileman/lib/bin/fileman-start.sh"]
