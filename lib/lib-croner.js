module.exports = function(app) {

    const MError = require("MonsterError")
    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()
    var dotq = require("MonsterDotq")
    var fs = dotq.fs();
    var lockFile = require("MonsterLockfile");
    var path = require("path")

    var request = require("RequestCommon")
    var croner = require("Croner")

    var cronStorePath = app.config.get("cron_store_directory_path")

    const webhostingFileRegex = new RegExp("^([0-9]+)\.json$")

    var lib = {}

    if(!app.cronTasks) app.cronTasks = {}

     vali.validators.isValidCronExpression = function(value, options, key, attributes, global) {

         if(!value)return
         var p = value.split(" ")
         if(p.length != 5) return "invalid number of parts in cron expression"
         if(!croner.validate(value)) return "invalid cron expression"
      }

     vali.validators.isValidType = function(value, options, key, attributes, global) {
         if(value == "url")
            extend(global.constraints, {data:{presence:true, url:true}})
     }


    lib.DeleteEntryById = function(in_data) {
      return doValidateAndRead(in_data, {cron_id: {presence:true, isString: true}})
        .then(re=>{
           destroyTask(re.d.cron_id, re)

           return writeCronFile(re)
        })

    }

    function findDupe(crons, candidate){
        var re;
        Object.keys(crons).some(function(cronId){
           var cron = crons[cronId];
           if((cron.cronExpression == candidate.cronExpression)&&(cron.type == candidate.type)&&(cron.data == candidate.data)) {
              re = cronId;
              return true;
           }

        })

        return re;

    }

    lib.Insert = function(webhosting, in_data) {
      var re
      return doValidateAndRead(in_data, {
        cron_id: {isString: {ascii:true, hex: true}},
        user_id: {presence: true, isValidUserId: {info:app.MonsterInfoAccount}},
        cronExpression: {presence:true, isValidCronExpression: true},
        type: {presence: true, inclusion: ['url'], isValidType:true}
      }, true)
        .then(are=>{
           re = are;
           var existingId = findDupe(re.cron, re.d);
           if(existingId) {
              re.id = existingId;
              return;
           }
           if((re.d.cron_id)&&(re.cron[re.d.cron_id])) {
              // an id was specified, but an entry with different content and the same id already exists
              throw new MError("COLLIDING_ENTRY")
           }


            return (re.d.cron_id ? Promise.resolve(re.d.cron_id) : require("Token").GetTokenAsync(8,"hex"))
              .then(id=>{
                 re.id = id
                 delete re.d.cron_id;
                 re.cron[id] = re.d

                 return writeCronFile(re)
              })
              .then(()=>{
                 return startCron(re.id, re.d)
              })
           
        })
        .then(()=>{
           return {cron_id:re.id};
        })
    }

    lib.GetWebstoreEntries = function(webhosting) {
      return doRead({wh_id:webhosting.wh_id}, true)
        .then(re=>{
           var x = {entries:re.cron}
           if(webhosting.template)
              x.canBeAdded = webhosting.template.t_cron_max_entries > Object.keys(re.cron).length
           return Promise.resolve(x)
        })
    }

    lib.GetAllWebhostings = function(){
       return getAllWebhostings();
    }

    lib.GetAllEntriesAggregated = function() {
       return getAllWebhostings()
         .then(whs=>{
             var re = {}
             return dotq.linearMap({array: whs, catch:true, action:function(wh){
                return lib.GetWebstoreEntries({wh_id: wh})
                  .then(x=>{
                      extend(re, x.entries)
                  })
             }})
             .then(()=>{
                return Promise.resolve({entries:re})
             })
         })
    }

    lib.DeleteAll = function(in_data) {
      return doValidateAndRead(in_data)
        .then(re=>{
           Object.keys(re.cron).forEach(task_id=>{
              destroyTask(task_id)
           })
           return fs.unlinkAsync(re.path)
        })
    }


    startAllCrons()

    return lib


    function startAllCrons(){
       if(app.crons_strted) return
        app.crons_started = true
        return lib.GetAllEntriesAggregated()
          .then(crons=>{
             Object.keys(crons.entries).forEach(cron_id=>{
                startCron(cron_id, crons.entries[cron_id])
             })
          })
    }

    function startCron(cron_id, details) {
       if(app.cronTasks[cron_id]) return;
       app.cronTasks[cron_id] = croner.schedule(details.cronExpression, function(){
          const moment = require("MonsterMoment");
          var now = moment.now();

          if(details.type == "url") {
              console.log("Fetching", details.data, "at", now)
              return request.PostForm(details.data, null, {devNull: true})
              .then(()=>{
                 console.log("Fetching", details.data, "succeded; started at", now)
              })
              .catch(err=>{
                 console.error("Unable to fetch", details.data, now, err)
              })
          }
       })
    }

    function writeCronFile(re){
         // and we need to save it now
         return lockFile.logic({lockfile:re.lockFileFn, callback:function(){
             return fs.writeFileAsync(re.path, JSON.stringify(re.cron))
         }})
    }

    function getAllWebhostings(){
       return fs.readdirAsync(cronStorePath)
         .then(files=>{
            var re = []
            files.forEach(file=>{
               var m = webhostingFileRegex.exec(file)
               if(!m) return
               re.push(m[1])
            })
            return Promise.resolve(re)
         })
    }

    function destroyTask(task_id, re){
       if(app.cronTasks[task_id]) {
         app.cronTasks[task_id].destroy()
         delete app.cronTasks[task_id]
       }

       if(re){
             if((!re.cron)||(!re.cron[re.d.cron_id]))
                throw new MError("ENTRY_NOT_FOUND")

             delete re.cron[re.d.cron_id]
       }
    }

    function doRead(d, catchReadError){
      var re = {d: d}
      re.path = path.join(cronStorePath, d.wh_id+".json")
      re.lockFileFn = "fileman-croner-"+d.wh_id
      return fs.readFileAsync(re.path)
        .then(c=>{
            re.cron = JSON.parse(c)
        })
        .catch(err=>{
          if(catchReadError) {
             console.error("Error while reading cron file", re.path, err)
             re.cron = {}
             return Promise.resolve()
          }
          throw err
        })
        .then(()=>{
            return Promise.resolve(re)
        })
    }

    function doValidate(in_data, additionalRules){
      return vali.async(in_data, extend({wh_id: {presence:true, isInteger: true}}, additionalRules))
    }
    function doValidateAndRead(in_data, additionalRules, catchReadError){
      return doValidate(in_data, additionalRules)
        .then(d=>{
           return doRead(d, catchReadError)
        })
    }


}
