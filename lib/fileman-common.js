module.exports = {

   filterStat: function(webhosting, name, stat, filters){

   	  if(!filters) filters = {}

        if(!filters.extraVisibleUids) filters.extraVisibleUids = []
        if(!Array.isArray(filters.extraVisibleUids)) filters.extraVisibleUids = [filters.extraVisibleUids]
        filters.extraVisibleUids.push(webhosting.wh_id)

   	  const Mode = require('stat-mode');
   	  var mode = new Mode(stat);

   	  if((!filters.dontFilterSymlinks)&&(mode.isSymbolicLink())) {
   	  	// console.log("this is a symbolic link!", name)
   	  	return
   	  }

   	  if((filters.dirsOnly)&&(!mode.isDirectory())) return
      if((filters.filesOnly)&&(!mode.isFile())) return        

   	  if((!filters.dontEnforceSameOwner) && (filters.extraVisibleUids.indexOf(stat.uid) < 0)) return

   	  return {
   	  	 name: name,
   	  	 perm: mode.toString(),
   	  	 mtime: stat.mtime,
   	  	 size: stat.size,
   	  }
   }
}
