module.exports = function(app) {

    var router = app.ExpressPromiseRouter()
    var cronLib = require("lib-croner.js")(app)


    router.use(function(req,res,next){
        req.cronParams = {}
        if(req.webhosting)
            req.cronParams.wh_id = req.webhosting.wh_id

        return next()
    })

    router.route("/:cron_id")
      .delete(function(req,res,next){
         req.cronParams.cron_id = req.params.cron_id
         return req.sendOk(
            cronLib.DeleteEntryById(req.cronParams)
              .then(()=>{
                  app.InsertEvent(req, {e_event_type: "cron-removed", e_other: true});
              })
         )
      })

    router.route("/")
      .put(function(req,res,next){
         return cronLib.Insert(req.webhosting, extend({}, req.body.json, req.cronParams))
              .then((r)=>{
                  app.InsertEvent(req, {e_event_type: "cron-insert", e_other: true});
                  return req.sendResponse(r);
              })
      })
      .get(function(req,res,next){
         var p
         if(req.webhosting){
            p = cronLib.GetWebstoreEntries(req.webhosting)
         } else {
            p = cronLib.GetAllEntriesAggregated()
         }
         return req.sendPromResultAsIs(p)
      })
      .delete(function(req,res,next){
         return req.sendOk(
            cronLib.DeleteAll(req.cronParams)
              .then(()=>{
                  app.InsertEvent(req, {e_event_type: "cron-removed", e_other: true});
              })
              .catch(ex=>{
                 if(ex.code != "ENOENT")
                   throw ex;

                 // this is fine, it is already removed.
              })
         )
      })

    return router

}
