require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


   var common = require("000-common.js")(app)
   common.setupInfo()


   describe("template", function(){


        it('template should work', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, 
                  { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        '-v',
        '[template_dir]:/mnt',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'template.sh',
        '[web_path_inside_chroot][dst_dir_path]' ] } }

    )

                  assert.ok(param2[2].extras.path_to_use);
                  delete param2[2].extras.path_to_use

                  assert.deepEqual(param2, [ { dst_dir_path: '/foo/bar/' },
                    {template_dir: "/path/to/template/"},
  { wh_id: '12345',template: { t_cron_max_entries: 1 }, run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
    }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/template", {dst_dir_path: "/foo/bar", template: "nocontent"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })

   });


	describe('mkdir', function() {



        it('mkdir should work', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'mkdir.sh',
        '[web_path_inside_chroot][dst_dir_path]' ] } })

                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use

                  assert.deepEqual(param2, [ { dst_dir_path: '/foo/bar/' },
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
    }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/mkdir", {dst_dir_path: "/foo/bar"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


	})



  describe('maildirmake', function() {



        it('maildirmake should work', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, { chain:
{ executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'maildirmake.sh',
        '[web_path_inside_chroot][dst_dir_path]' ] } })

                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use

                  assert.deepEqual(param2, [ { dst_dir_path: '/foo/bar/' },
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("maildirmake_run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/maildirmake", {dst_dir_path: "/foo/bar"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


  })



  describe('move', function() {



        it('move should work', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, { chain:
{ executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'move.sh',
        '[web_path_inside_chroot]',
        'some/file1.txt',
        'some/file2.txt',
        'foo/bar' ] } })

                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use

                  assert.deepEqual(param2, [
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/move", {src_pathes: ["/some/file1.txt", "/some/file2.txt"], dst_dir_path: "/foo/bar"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


        it('move should work back to root as well', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                   console.log("hey", param1, param2)

                  assert.deepEqual(param1, { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'move.sh',
        '[web_path_inside_chroot]',
        'some/file1.txt',
        'some/file2.txt',
        '.' ] } }) // note the dot here!

                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use

                  assert.deepEqual(param2, [
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
            extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/move", {src_pathes: ["/some/file1.txt", "/some/file2.txt"], dst_dir_path: "/"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


  })





  describe('copy', function() {



        it('copy should work', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, { chain:
{ executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'copy.sh',
        '[web_path_inside_chroot]',
        'some/file1.txt',
        'some/file2.txt',
        'foo/bar' ] } })

                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use

                  assert.deepEqual(param2, [
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/copy", {src_pathes: ["/some/file1.txt", "/some/file2.txt"], dst_dir_path: "/foo/bar"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


        it('copy should work back to root as well', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                   console.log("hey", param1, param2)

                  assert.deepEqual(param1, { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'copy.sh',
        '[web_path_inside_chroot]',
        'some/file1.txt',
        'some/file2.txt',
        '.' ] } }) // note the dot here!

                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use

                  assert.deepEqual(param2, [
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
            extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/copy", {src_pathes: ["/some/file1.txt", "/some/file2.txt"], dst_dir_path: "/"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


  })

  describe('find', function() {



        it('using find to delete files older than 2 days and having name *.txt ', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1,  { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'find.sh',
        '[web_path_inside_chroot][path]',
        '-mtime',
        '+2',
        '-name',
        '*.txt',
        '-delete',
        '-print' ] } })

                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use

                  assert.deepEqual(param2, [ { path: '/foo/bar/' },
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     } ,
  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/find", {path: "/foo/bar",name:"*.txt",mtime:"+2",delete:true}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


  })


  describe("wiping (before final removal of a webstore)", function(){


        it('wipe should work', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, 
                   { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'wipe.sh',
        '[web_path_inside_chroot]' ] } }
    )

                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use

                  assert.deepEqual(param2, [ 
  { wh_id: '12345',template: { t_cron_max_entries: 1 }, run_script_as_uid: '0', run_script_as_gid: '0',
    extras: { web_path: "test/storage",
    }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])

                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/wipe", {}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


  })

}, 10000)


