module.exports = function(app) {
  var latestImages = require("./latest-images.json");

  return {
    dockerImageVersions: latestImages,
	setupInfo: function(){
	    app.MonsterInfoWebhosting = {
	        GetInfo: function(storageId){

	           return Promise.resolve({wh_id: storageId, template:{t_cron_max_entries:1}, extras: {web_path: "test/storage"}})

	        }
	    }
	},
    setupAccountInfo: function(user_id, assert){
         user_id = ""+user_id

            app.MonsterInfoAccount = {
                GetInfo: function(account_id){
                    assert.equal(account_id, user_id)
                    return Promise.resolve({"u_id":user_id})
                }
            }

      }
  }
}
