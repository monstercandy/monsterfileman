require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

   const dupeWebhostingId = 10003;

   var webhostingIds = ["10001","10002"]
   const user_id = "21854"

   const expectedBackup = { cronExpression: '0 0 * * *',
        type: 'url',
        data: 'http://index.hu/foobar' };


   var common = require("000-common.js")(app)
   common.setupInfo()
   common.setupAccountInfo(user_id, assert)

        var cron_id

	describe('cron urls', function() {

        shouldBeEmpty()

        it("invalid cron expression should be rejected (60)", function(done){

           mapi.put("/"+webhostingIds[0]+"/cron", {user_id: user_id, cronExpression: "60 0 * * *", type:"url", data: "http://index.hu/foobar"}, function(err,result){
              assert.propertyVal(err, "message", "VALIDATION_ERROR")
              done()
           })

        })

        it("invalid cron expression should be rejected (second)", function(done){

           mapi.put("/"+webhostingIds[0]+"/cron", {user_id: user_id, cronExpression: "0 0 0 * * *", type:"url", data: "http://index.hu/foobar"}, function(err,result){
              assert.propertyVal(err, "message", "VALIDATION_ERROR")
              done()
           })

        })


        insertEntry()

        it("and now it should be returned", function(done){
            mapi.get("/"+webhostingIds[0]+"/cron", function(err,result){
               assert.propertyVal(result, "canBeAdded", false)
               assert.property(result, "entries")
               assert.property(result.entries, cron_id)
               assert.deepEqual(result.entries[cron_id], {user_id: user_id,  cronExpression: '0 0 * * *', type: 'url', data: 'http://index.hu/foobar', wh_id: '10001' })
               done()
            })
        })

        it("removing it should be working", function(done){

           mapi.delete("/"+webhostingIds[0]+"/cron/"+cron_id, {}, function(err,result){
              assert.equal(result, "ok")
              done()
           })

        })


        shouldBeEmpty()

        insertEntry()
        insertEntry(webhostingIds[1])

        it("both of them should be returned through the superuser route", function(done){
              mapi.get("/cron", function(err,result){
                 // console.log(result)
                 assert.property(result, "entries")
                 assert.equal(Object.keys(result.entries).length, 2)
                 assert.deepEqual(Object.values(result.entries).map(x=>x.wh_id), webhostingIds)
                 done()
              })

        })

        webhostingIds.forEach(wh_id=>{
          it("removing the whole stuff should be working via: /"+wh_id+"/cron/", function(done){

             mapi.delete("/"+wh_id+"/cron/", {}, function(err,result){
                assert.equal(result, "ok")
                done()
             })

          })

        })


        shouldBeEmpty()


  })


  describe("dupe checking", function(){

     it("configuration", function(){
          app.MonsterInfoWebhosting = {
              GetInfo: function(storageId){

                 return Promise.resolve({wh_id: storageId, wh_user_id: 21854, template:{t_cron_max_entries:2}, extras: {web_path: "test/storage"}})

              }
          }      
     })

     insertEntry(dupeWebhostingId);
     insertEntry(dupeWebhostingId);

        it("and now it should be returned", function(done){
            mapi.get("/"+dupeWebhostingId+"/cron", function(err,result){
               assert.propertyVal(result, "canBeAdded", true)
               assert.property(result, "entries");
               assert.equal(Object.keys(result.entries).length, 1);
               done()
            })
        })

  })



  var latestBackup;
  var latestBackupId;
  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

      cleanup();

        it('get list of ftp accounts should be empty', function(done) {
            mapi.get("/"+dupeWebhostingId+"/cron", function(err,result){
               assert.propertyVal(result, "canBeAdded", true)
               assert.property(result, "entries");
               assert.deepEqual(result.entries, {});
               done()
            })
        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });


  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

             mapi.post( "/wie/"+dupeWebhostingId, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })

        })
  }


  function cleanup(){
      it("cleanup first", function(done){
             mapi.delete( "/"+dupeWebhostingId+"/cron/", {}, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })
      })
  }

  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+dupeWebhostingId, function(err, result, httpResponse){

                latestBackup = simpleCloneObject(result);

                var cronIds = Object.keys(result.entries);
                if(latestBackupId)
                   assert.equal(cronIds[0], latestBackupId);
                else
                  latestBackupId = cronIds[0];

                assert.deepEqual(latestBackup.entries[latestBackupId], expectedBackup);
                done()

             })

        })
  }



  function shouldBeEmpty(){
      it("should be empty", function(done){
             mapi.get( "/wie/"+dupeWebhostingId, function(err, result, httpResponse){

                latestBackup = simpleCloneObject(result);

                var cronIds = Object.keys(latestBackup.entries);
                assert.equal(cronIds.length, 0);
                done()

             })

      })
  }

  function insertEntry(wh_id){
        var url = "/"+(wh_id||webhostingIds[0])+"/cron";
        it("inserting a new cron rule should be cool: "+url, function(done){

           mapi.put(url, {user_id: user_id, cronExpression: "0 0 * * *", type:"url", data: "http://index.hu/foobar"}, function(err,result){
             assert.property(result, "cron_id")
             cron_id =result.cron_id
             assert.ok(cron_id)
             done()
           })

        })

  }

}, 10000)


