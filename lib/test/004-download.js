require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


   var common = require("000-common.js")(app)
   common.setupInfo()


	describe('upload', function() {


        it('file download should be rejected with invalid filename', function(done) {

             mapi.post( "/12345/download", {src_file: "/../crap"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        it('file download should succeed with correct filename', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, { 
                   downloadAs: "crap.txt",
                   chain:
    { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'download.sh',
        '[web_path_inside_chroot][src_file]' ] },
  executeImmediately: true,
  download: true })

                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, [ { src_file: '/foo/crap.txt' },
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/download", {src_file: "/foo/crap.txt"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


        it('tar download should be rejected with invalid filename', function(done) {

             mapi.post( "/12345/download", {pathes: ["/correct.txt","/../crap"]}, function(err, result, httpResponse){
                //  console.log(result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })


        it('tar download should succeed with correct filename', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                   // console.log("hey", param1, param2)

                  assert.property(param1, "chain")
                  assert.property(param1, "downloadAs")
                  assert.ok(param1, "downloadAs")
                  assert.ok(param1.downloadAs.match(/\.tar$/))
                  assert.propertyVal(param1, "download", true)
                  assert.deepEqual(param1.chain,
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'tar.sh',
        '[web_path_inside_chroot][dst_dir_path]',
        '[tar_params]',
        'foo/crap.txt',
        'foo/crap2.txt' ] })


                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, [
                    {"dst_dir_path": "/", "tar_params":""},
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/tar", {dst_dir_path: "/", pathes:["/foo/crap.txt","/foo/crap2.txt"]}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })



        it('taring the complete site should work', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                   console.log("hey", param1, param2)

                  assert.property(param1, "chain")
                  assert.property(param1, "downloadAs")
                  assert.ok(param1, "downloadAs")
                  assert.ok(param1.downloadAs.match(/\.tgz$/))
                  assert.propertyVal(param1, "download", true)
                  assert.deepEqual(param1.chain,
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'tgz-all.sh',
        '[web_path_inside_chroot]' ] })


                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use;

                  assert.deepEqual(param2, [
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/tgz-all", {}, function(err, result, httpResponse){
                // console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


        it('delbackup test', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                   console.log("hey", param1, param2)

                  assert.property(param1, "chain")
                  assert.property(param1, "downloadAs")
                  assert.propertyVal(param1, "download", true)
                  assert.deepEqual(param1.chain,
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'delbackup.sh',
        '[web_path_inside_chroot]',
        '[delbackup_key_or_supplied]',
        '--exclude=weblogs/*',
        '--exclude=sessions/*',
        '--exclude=_mm_o/*',
        '--exclude=tmp/*' ] })


                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, [ {delbackup_key_or_supplied:"abcd1234"},
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
     extras: { web_path: "test/storage",
     }
  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/delbackup", {password:"abcd1234"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


        it('zip creation should work', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  console.log("hey", param1, param2)

                  assert.property(param1, "chain")
                  assert.notProperty(param1, "downloadAs")
                  assert.notProperty(param1, "download")
                  assert.deepEqual(param1.chain,
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'zip.sh',
        '[web_path_inside_chroot][dst_dir_path]',
        '[dst_zip_name]',
        'foo/crap.txt',
        'foo/crap2.txt' ] })


                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, [
                    {"dst_dir_path": "/", "dst_zip_name": "abcd.zip", "tar_params":""},
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/zip", {dst_dir_path: "/", dst_zip_name: "abcd.zip", pathes:["/foo/crap.txt","/foo/crap2.txt"]}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


	})



}, 10000)


