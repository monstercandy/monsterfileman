require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


   var common = require("000-common.js")(app)
   common.setupInfo()


	describe('upload', function() {



        it('file upload should be rejected with invalid filename', function(done) {

             mapi.post( "/12345/upload", {dst_full_path: "/../crap"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        it('file upload should succeed with correct filename', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1,{ upload: true, executeImmediately: true, chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'upload.sh',
        '[web_path_inside_chroot][dst_full_path]' ] } })

                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;


                  assert.deepEqual(param2, [ { dst_full_path: '/foo/crap.txt' },
  { wh_id: '12345', template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { 
        web_path: "test/storage",
     }
  },
    app.config.get("cmd_dependencies") ,
    common.dockerImageVersions
  ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/upload", {dst_full_path: "/foo/crap.txt"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })






        it('untgz should succeed with correct params', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1,{ dontLog_stdout: true, omitAggregatedOutput: true, upload: true, chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'untar.sh',
        '[web_path_inside_chroot][dst_dir_path]',
        '[tar_params]' ] } })


                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, [ { dst_dir_path: '/foo/', tar_params: 'z' },
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }
     },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/untgz", {dst_dir_path: "/foo", filename: "something.tar.gz"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })




        it('unzip should succeed with correct params', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                  // console.log("hey", param1, param2)

                  assert.deepEqual(param1, {dontLog_stdout: true, omitAggregatedOutput:true, upload: true, chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'unzip.sh',
        '[web_path_inside_chroot]',
        '[dst_dir_path]' ] } })


                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, [ { dst_dir_path: '/foo/' },
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }
     },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions ])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/unzip", {dst_dir_path: "/foo", filename: "something.tar.gz"}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


	})



}, 10000)


