require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


   var common = require("000-common.js")(app)
   common.setupInfo()


	describe('remove', function() {



        it('file remove should be rejected with invalid filenames', function(done) {

             mapi.post( "/12345/remove", {pathes: ["/valid/stuff.txt","/../crap"]}, function(err, result, httpResponse){
                //  console.log(result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })



        it('file remove should throw WEBSTORE_ROOT_DIR_NOT_FOUND exception when it is not found', function(done) {
           var oMonsterInfoWebhosting = app.MonsterInfoWebhosting

            app.MonsterInfoWebhosting = {
                GetInfo: function(storageId){

                   return Promise.resolve({wh_id: storageId, extras: {web_path: "test/storage_not_found"}})

                }
            }

             mapi.post( "/12345/remove", {pathes: ["/valid/stuff.txt"]}, function(err, result, httpResponse){
                //  console.log(result)
                assert.propertyVal(err, "message", "WEBSTORE_ROOT_DIR_NOT_FOUND")
                app.MonsterInfoWebhosting = oMonsterInfoWebhosting
                done()

             })

        })

        it('file remove should succeed with correct filename', function(done) {


          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                   console.log("hey", param1, param2)

                  assert.deepEqual(param1,{
                   chain:
    { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'remove.sh',
        '[web_path_inside_chroot]',
        'foo/crap.txt',
        'foo2/crap2.txt' ] } })


                  assert.ok(param2[0].extras.path_to_use);
                  delete param2[0].extras.path_to_use

                  assert.deepEqual(param2, [
  { wh_id: '12345',template: { t_cron_max_entries: 1 },run_script_as_uid: '12345', run_script_as_gid: app.config.get("run_script_as_gid"),
    extras: { web_path: "test/storage",
     }  },
  app.config.get("cmd_dependencies"),
    common.dockerImageVersions])


                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/remove", {pathes: ["/foo/crap.txt", "/foo2/crap2.txt"]}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })




	})



}, 10000)


