require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


   var common = require("000-common.js")(app)
   common.setupInfo()

	describe('chattr', function() {



        it('is readonly should trigger a file stat', function(done) {

             mapi.get( "/12345/isreadonly", function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.equal(result, false)
                done()

             })

        })

        it('chattr +i should invoke the script as root', function(done) {

          const id = "foobar"

            app.commander = {
               spawn: function(param1, param2){
                 console.log("hey1", param1)

                  assert.deepEqual(param1, { chain:
   { executable: 'docker',
     args:
      [ 'run',
        '--rm',
        "-i",
        '-u',
        '[run_script_as_uid]:[run_script_as_gid]',
        '-v',
        '[extras.path_to_use]:[web_path_inside_chroot]',
        "--privileged",
        'alpine-3.7-fileman:[alpine-37-fileman]',
        'chattr.pl',
        '[wh_id]',
        '[p_or_m]',
        '[web_path_inside_chroot]' ] } })

                  var expectation2 = [ { p_or_m: 'p' },
    { wh_id: '12345',
    template: { t_cron_max_entries: 1 },
    run_script_as_uid: "0", // note the zero
    run_script_as_gid: "0", // note the zero
    extras: {
       web_path: "test/storage",
     } 
    },
    app.config.get("cmd_dependencies"),
    common.dockerImageVersions
  ];
                  // console.log("hey2", param2, expectation2)

                  assert.ok(param2[1].extras.path_to_use);
                  delete param2[1].extras.path_to_use;

                  assert.deepEqual(param2, expectation2)

                  return Promise.resolve({id:id, executionPromise: Promise.resolve()})

               }
            }

             mapi.post( "/12345/toreadonly", {}, function(err, result, httpResponse){
                //  console.log(result)
                assert.isNull(err)
                assert.deepEqual(result, { id: id })
                done()

             })

        })


	})



}, 10000)


