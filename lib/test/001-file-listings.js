require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../fileman-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

   var common = require("000-common.js")(app)

   common.setupInfo()



	describe('dir listings', function() {
        const common = require("fileman-common.js")

        it("file filter reject (not same owner)", function(){

               var stat = { dev: 1585324392,
                  mode: 33206,
                  nlink: 1,
                  uid: 10000,
                  gid: 0,
                  rdev: 0,
                  blksize: undefined,
                  ino: 55169095435489760,
                  size: 5,
                  blocks: undefined,
                  atime: new Date(),
                  mtime: new Date(),
                  ctime: new Date(),
                  birthtime: new Date() }


            var r = common.filterStat({wh_id: 10001}, "foo", stat)
            assert.isUndefined(r)
        })

        it("file filter accept (same owner)", function(){

               var stat = { dev: 1585324392,
                  mode: 33206,
                  nlink: 1,
                  uid: 10001,
                  gid: 0,
                  rdev: 0,
                  blksize: undefined,
                  ino: 55169095435489760,
                  size: 5,
                  blocks: undefined,
                  atime: new Date(),
                  mtime: "mtime",
                  ctime: new Date(),
                  birthtime: new Date() }


            var r = common.filterStat({wh_id: 10001}, "foo", stat)
            assert.deepEqual(r, {name: "foo", mtime: "mtime", perm: "-rw-rw-rw-", size: 5})
        })

        it("file filter accept (same owner)", function(){

               var stat = { dev: 1585324392,
                  mode: 33206,
                  nlink: 1,
                  uid: 10000,
                  gid: 0,
                  rdev: 0,
                  blksize: undefined,
                  ino: 55169095435489760,
                  size: 5,
                  blocks: undefined,
                  atime: new Date(),
                  mtime: "mtime",
                  ctime: new Date(),
                  birthtime: new Date() }


            var r = common.filterStat({wh_id: 12345}, "foo", stat, {extraVisibleUids: 10000})
            assert.deepEqual(r, {name: "foo", mtime: "mtime", perm: "-rw-rw-rw-", size: 5})
        })


    Array("crap","",undefined, null, "/../../../", "/legit/../../").forEach(path=>{

        it('cwd should reject invalid pathes: '+path, function(done) {

             mapi.post( "/12345/list", {"cwd": path}, function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })


    })



        it('by default no entries should be returned due to same owner filtering', function(done) {

             mapi.post( "/12345/list", {"cwd": "/"}, function(err, result, httpResponse){
                //  console.log(result)
                //  console.log(result.error.params.cwd)
                assert.isNull(err)
                assert.deepEqual(result, [  ])
                done()

             })

        })

        it('should return stuffs inside', function(done) {

           process.env.DONT_ENFORCE_SAME_OWNER = true

             mapi.post( "/12345/list", {"cwd": "/"}, function(err, result, httpResponse){
                // console.log(result)
                assert.isNull(err);

                result.forEach(r=>{
                   assert.ok(r.mtime);
                   delete r.mtime;
                });

                sortResponse(result);

                // console.log(result);

                assert.deepEqual(result, [ 
  { name: 'hello.txt',
    perm: '-rw-rw-rw-',
    size: 5 },
  { name: 'subdir',
    perm: 'drw-rw-rw-',
    size: 0 },
  { name: 'world.txt',
    perm: '-rw-rw-rw-',
    size: 5 } ])
                done()

             })

        })

        it('should return stuffs inside, files only', function(done) {

             mapi.post( "/12345/list", {"cwd": "/", filesOnly: true}, function(err, result, httpResponse){
                // console.log(result)
                assert.isNull(err)
                for(var i = 0; i < 2; i++) {
                  assert.ok(result[i].mtime);
                  delete result[i].mtime;
                }
                sortResponse(result);                
                assert.deepEqual(result, [{
          "name": "hello.txt",
          "perm": "-rw-rw-rw-",
          "size": 5
        },
  { name: 'world.txt',
    perm: '-rw-rw-rw-',
    size: 5 } ])
                done()

             })

        })

        it('should return stuffs inside, dirs only', function(done) {

             mapi.post( "/12345/list", {"cwd": "/", dirsOnly: true}, function(err, result, httpResponse){
                  console.log(result)
                assert.isNull(err)
                assert.ok(result[0].mtime);
                delete result[0].mtime;
                sortResponse(result);                

                assert.deepEqual(result, [
  { name: 'subdir',
    perm: 'drw-rw-rw-',
    size: 0 }])
                done()

             })

        })

        it('subdirs should be good too', function(done) {

             mapi.post( "/12345/list", {"cwd": "/subdir"}, function(err, result, httpResponse){
                // console.log(result)
                assert.isNull(err)
                assert.ok(result[0].mtime);
                delete result[0].mtime;
                assert.deepEqual(result, [ { name: 'subfile.txt',
    perm: '-rw-rw-rw-',
    size: 7 } ])
                done()

             })

        })

        it('breaking out via a symlink should not be possible', function(done) {

             mapi.post( "/12345/list", {"cwd": "/foobar"}, function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(err, "message", "INVALID_PATH")
                done()

             })

        })

        it('non existing cwd', function(done) {

             mapi.post( "/12345/list", {"cwd": "/non_exist"}, function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(err, "message", "PATH_NOT_FOUND")
                done()

             })

        })

	})


  function sortResponse(result){
        function strcmp(a, b)
        {   
            return (a<b?-1:(a>b?1:0));  
        }

        result.sort((r1,r2)=>{return strcmp(r1.name, r2.name)});
  }

}, 10000)


