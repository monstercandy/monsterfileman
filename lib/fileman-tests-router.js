module.exports = function(app) {

    const MError = require("MonsterError")

    var router = app.ExpressPromiseRouter()

    const perl = "/usr/bin/perl"
    const basedir = "/opt/MonsterCommon/lib/Commander/test/"

    router.post("/upload", function(req,res,next){

       return req.sendTask(
            app.commander.spawn({upload: true, executeImmediately: true, chain:{executable:perl,args:[basedir+"sha1.pl"]}})
       )

    })

    router.post("/download", function(req,res,next){

       return req.sendTask(
            app.commander.spawn({download: true, executeImmediately: true, chain:{stdin:'',executable:perl,args:[basedir+"manylines-agressive.pl"]}})
       )

    })

    router.post("/longread-simple", function(req,res,next){

       return req.sendTask(
            app.commander.spawn({omitAggregatedOutput: true, executeImmediately: true, chain:{stdin:'simple',executable:perl,args:[basedir+"manylines-agressive.pl"]}})
       )

    })

    router.post("/longread-upload", function(req,res,next){

       return req.sendTask(
            app.commander.spawn({omitAggregatedOutput: true, upload:true, executeImmediately: true, chain:{executable:perl,args:[basedir+"manylines-agressive.pl"]}})
       )

    })


    router.get("/tasks", function(req,res,next){
        return req.sendResponse(app.commander.getUnfinishedTaskIds())
    })

    return router



}
