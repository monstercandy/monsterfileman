module.exports = function(app) {

    const path = require("path").posix
    const MError = require("MonsterError")
    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

    var router = app.ExpressPromiseRouter()

    var fs = require("MonsterDotq").fs();

    const common = require("fileman-common.js")


    router.use("/cron", require("fileman-cron-router.js")(app))


    router.get("/tasks", function(req,res,next){
        req.sendResponse(app.perWebhostingTasks[req.webhosting.wh_id] || {})
    })

    router.get("/isreadonly", function(req,res,next){
        return req.sendPromResultAsIs(isReadonly(req.webhosting))
    })
    router.post("/toreadonly", function(req,res,next){
        return toReadOnlyOrReadWrite(req, "p")
    })
    router.post("/toreadwrite", function(req,res,next){
        return toReadOnlyOrReadWrite(req, "m")
    })


    router.post("/template", function(req,res,next){

         var templates = app.config.get("templates");


         return vali.async(req.body.json, {
            dst_dir_path: {presence:true, isPath: {noPathCharacterRestrictions: true}},
            template: {presence: true, inclusion: Object.keys(templates)}
         })
         .then(d=>{
             var cmd_template = simpleCloneObject(app.config.get("cmd_template"));

             app.InsertEvent(req, {e_event_type: "template", e_other: true});
             return req.sendTask(
                  spawnThrough(req.webhosting)({
                    chain:cmd_template,
                }, [
                  {dst_dir_path: d.dst_dir_path}, 
                  {template_dir: templates[d.template]}
                ])
             )
         })

    })

    router.post("/maildirmake", function(req,res,next){

         var maildirmake = app.config.get("cmd_maildirmake")

         return vali.async(req.body.json, {
            dst_dir_path: {presence:true, isPath: {noPathCharacterRestrictions: true}}
         })
         .then(d=>{
             req.webhosting.run_script_as_gid = app.config.get("maildirmake_run_script_as_gid")
             app.InsertEvent(req, {e_event_type: "maildirmake", e_other: true});
             return req.sendTask(
                  spawnThrough(req.webhosting)({
                    chain:maildirmake,
                }, [{dst_dir_path: d.dst_dir_path}])
             )
         })

    })

    // fetch is the lightweight version of download, it does not require asynchronous task processing
    // but the response is limited to avoid DoS attacks
    router.post("/fetch", function(req,res,next){

         var download = app.config.get("cmd_download")

         return vali.async(req.body.json, {
            src_file: {presence:true, isPath: {stripTailingSlash:true, noPathCharacterRestrictions: true}}
         })
         .then(d=>{

             app.InsertEvent(req, {e_event_type: "download", e_other: true});

             var re = "";
             var soFar = 0;
             var taskId;
             var aborted = false;
             var maxBytes = app.config.get("fetch_max_bytes");
             return spawnThrough(req.webhosting)({
                    chain:download,
                    executeImmediately: true,
                    omitAggregatedOutput: true,
                    dontLog_stdout: true,
                    stdoutReader: function(data){
                       if(aborted) return;
                       soFar += data.length;
                       if(soFar > maxBytes) {
                          aborted = true;
                          app.commander.abortTask(taskId);

                          return next(new MError("FILE_SIZE_EXCEEDED"));
                       } else {
                          re += data;
                       }
                    }
                }, 
                [
                  {src_file: d.src_file}
                ]
              )
              .then(d=>{
                  taskId = d.id;
                  return d.executionPromise;
              })
              .then(()=>{
                  return req.sendResponse({data: re});
              })
              .catch(ex=>{
                  if(aborted) return;
                  return next(new MError("ERROR_WHILE_FETCHING"));
              })
         })

    })

    router.post("/download", function(req,res,next){

         var download = app.config.get("cmd_download")

         return vali.async(req.body.json, {
            src_file: {presence:true, isPath: {stripTailingSlash:true, noPathCharacterRestrictions: true}}
         })
         .then(d=>{

             app.InsertEvent(req, {e_event_type: "download", e_other: true});
             return req.sendTask(
                  spawnThrough(req.webhosting)({
                    chain:download,
                    executeImmediately: true,
                    download: true,
                    downloadAs: path.basename(d.src_file)
                }, [{src_file: d.src_file}])
             )
         })

    })

    router.post("/upload", function(req,res,next){

         var upload = app.config.get("cmd_upload")

         return vali.async(req.body.json, {dst_full_path: {presence:true, isPath: {stripTailingSlash:true, noPathCharacterRestrictions: true}}})
           .then(d=>{
               app.InsertEvent(req, {e_event_type: "upload", e_other: true});
               return req.sendTask(
                    spawnThrough(req.webhosting)({upload: true, executeImmediately: true, chain:upload}, [{dst_full_path: d.dst_full_path}])
               )
           })

    })

    Array("untar","untgz").forEach(c=>{
      var untar = app.config.get("cmd_untar")
      router.post("/"+c, function(req,res,next){


           return vali.async(req.body.json, {
              filename: {presence: true, isFilename: {noPathCharacterRestrictions: true}},
              dst_dir_path: {presence:true, isPath: {noPathCharacterRestrictions: true}}
           })
           .then(d=>{
              var tar_params = ""
              if(c=="untgz")
                tar_params = "z"

               app.InsertEvent(req, {e_event_type: c, e_other: true});

               return req.sendTask(
                    spawnThrough(req.webhosting)({dontLog_stdout: true, omitAggregatedOutput: true, upload: true, chain:untar}, [{dst_dir_path: d.dst_dir_path, tar_params: tar_params}])
               )
           })

      })

    })

    router.post("/unzip", function(req,res,next){

         var unzip = app.config.get("cmd_unzip")

         return vali.async(req.body.json, {
            dst_dir_path: {presence:true, isPath: {noPathCharacterRestrictions: true}}
         })
         .then(d=>{

             app.InsertEvent(req, {e_event_type: "unzip", e_other: true});

             return req.sendTask(
                  spawnThrough(req.webhosting)({dontLog_stdout: true, omitAggregatedOutput: true, upload: true, chain:unzip}, [{dst_dir_path: d.dst_dir_path}])
             )
         })

    })


    router.post("/delbackup", function(req,res,next){


         return vali.async(req.body.json, {
            password: {isString: true}
         })
         .then(d=>{
             var excludes = simpleCloneObject(app.config.get("delbackup_tar_excludes"))
             var delbackup = simpleCloneObject(app.config.get("cmd_delbackup"))

             excludes = excludes.map(function(x){return "--exclude="+x+"/*"})

             delbackup.args = delbackup.args.concat(excludes)

             const moment = require("MonsterMoment")
             var nowStr = moment.nowRaw()

             app.InsertEvent(req, {e_event_type: "delbackup", e_other: true});

             return req.sendTask(
                  spawnThrough(req.webhosting)({
                    chain:delbackup,
                    download: true,
                    downloadAs: req.webhosting.wh_id+"-"+nowStr+".tgz"
                  }, [{delbackup_key_or_supplied: d.password || app.config.get("cmd_dependencies").delbackup_key}])
             )
         })

    })

    Array("tar","tgz","zip").forEach(cat=>{
        router.post("/"+cat, function(req,res,next){
             var constraints = {
                pathes: {presence:true, isPathArray: {lazy: true, stripTailingSlash:true, stripHeadingSlash:true, noPathCharacterRestrictions: true}},
                dst_dir_path: {isPath: {noPathCharacterRestrictions: true}, default: "/"}, 
             };
             if(cat == "zip") {
               constraints.dst_zip_name = {presence: true, isString: true};
             }
             return vali.async(req.body.json, constraints)
             .then(d=>{
                 if((d.dst_zip_name)&&(!d.dst_zip_name.match(/\.zip$/)))
                   d.dst_zip_name += ".zip";

                 var cmd_name = cat == "zip" ? "cmd_zip" : "cmd_tar";
                 var tar = simpleCloneObject(app.config.get(cmd_name));
                 tar.args = tar.args.concat(d.pathes);

                 var tar_params = "";
                 if(cat=="tgz")
                    tar_params = "z";

                 const moment = require("MonsterMoment");
                 var nowStr = moment.nowRaw();

                 app.InsertEvent(req, {e_event_type: cat, e_other: true});

                 delete d.pathes;
                 d.tar_params = tar_params;

                 var spawn = {
                        chain:tar,
                 };
                 if(cat != "zip") {
                   spawn.download = true;
                   spawn.downloadAs = req.webhosting.wh_id+"-"+nowStr+"."+cat;
                 }

                 return req.sendTask(
                      spawnThrough(req.webhosting)(spawn, [d])
                 )
             })

        })

    })

        router.post("/tgz-all", function(req,res,next){


                 var tar = app.config.get("cmd_tgz_all")

                 const moment = require("MonsterMoment")
                 var nowStr = moment.nowRaw()

                 app.InsertEvent(req, {e_event_type: "tgz", e_other: true});

                 return req.sendTask(
                      spawnThrough(req.webhosting)({
                        chain:tar,
                        dontRelayStderr: true,
                        download: true,
                        downloadAs: req.webhosting.wh_id+"-"+nowStr+".tgz"
                      }, [])
                 )

        })


    // this is to wipe the complete webstore (including root created files there)
    // mountpoints are checked for safety
    router.post("/wipe", function(req,res,next){
        var wipe = simpleCloneObject(app.config.get("cmd_wipe"));

        var spawner = spawnThrough(req.webhosting)
        req.webhosting.run_script_as_uid = "0" // run as root
        req.webhosting.run_script_as_gid = "0" // run as root
        app.InsertEvent(req, {e_event_type: "wipe", e_other: true});

        return req.sendTask(
            spawner({chain:wipe}, [])
        )
    })

    // this is to clean up all user created files on the webstore
    router.post("/cleanup", function(req,res,next){


             var cleanup = simpleCloneObject(app.config.get("cmd_cleanup"))

             app.InsertEvent(req, {e_event_type: "cleanup", e_other: true});

             return req.sendTask(
                  spawnThrough(req.webhosting)({chain:cleanup}, [])
             )

    })

    // this is to remove selected files
    router.post("/remove", function(req,res,next){


         return vali.async(req.body.json, {
            pathes: {presence:true, isPathArray: {stripTailingSlash:true, stripHeadingSlash:true, noPathCharacterRestrictions: true}}
         })
         .then(d=>{
             var remove = simpleCloneObject(app.config.get("cmd_remove"))
             remove.args = remove.args.concat(d.pathes)

             app.InsertEvent(req, {e_event_type: "remove", e_other: true});

             return req.sendTask(
                  spawnThrough(req.webhosting)({chain:remove}, [])
             )
         })

    })


    /*
    * NOTE: this is implemented in node, without any privilege dropping.
    * We still consider this to be safe, because files owned by anyone else
    * will be silently discarder and wont be visible in the result.
    */
    router.post("/list", workingDir, function(req,res,next){
       // console.log("hey", req.webhosting.extras.path_to_use)
       var fp = path.join(req.webhosting.extras.path_to_use, req.cwd)
       var re = []

       var p = req.cwd != "/" ? resolveAndCheckAgainstWebhosting(req.webhosting, fp) : Promise.resolve()

       p = p.then(()=>{
          return fs.readdirAsync(fp)
       })
       .then(filenames=>{
           var ps = []
           filenames.forEach(filename=>{
              var ffp = path.join(fp, filename)
              ps.push(
                 fs.lstatAsync(ffp)
                 .then(statStuff=>{
                    var f = common.filterStat(req.webhosting, filename, statStuff, {
                      extraVisibleUids: app.config.get("extra_visible_uids"),
                      filesOnly: req.body.json.filesOnly,
                      dirsOnly: req.body.json.dirsOnly,
                      dontEnforceSameOwner: process.env.DONT_ENFORCE_SAME_OWNER
                    })
                    if(f)
                      re.push(f)
                 })
                 .catch(ex=>{
                    console.error("could not stat:", ffp, ex)
                 })
              )
           })
           return Promise.all(ps)
       })
       .then(()=>{
           return Promise.resolve(re)
       })


       return req.sendPromResultAsIs(p)
    })

    router.post("/find", function(req,res,next){
         return vali.async(req.body.json, {
            path: {presence:true, isPath: {noPathCharacterRestrictions: true}},
            mtime: {isString: true, format: {pattern: /^(\+|-)?\d+$/}},
            name: {isString: true},
            delete: {isBooleanLazy: true}
         })
         .then(d=>{
             var find = simpleCloneObject(app.config.get("cmd_find"))
             if(d.mtime)
               find.args = find.args.concat(["-mtime", d.mtime])
             if(d.name)
               find.args = find.args.concat(["-name", d.name])
             if(d.delete)
               find.args.push("-delete")
             find.args.push("-print")

             return req.sendTask(
                  spawnThrough(req.webhosting)({
                    chain:find,
                  }, [{path: d.path}])
             )
         })

    })

    router.post("/mkdir", function(req,res,next){
         return vali.async(req.body.json, {
            dst_dir_path: {presence:true, isPath: {noPathCharacterRestrictions: true}}
         })
         .then(d=>{
             var mkdir = app.config.get("cmd_mkdir")

             app.InsertEvent(req, {e_event_type: "mkdir", e_other: true});

             return req.sendTask(
                  spawnThrough(req.webhosting)({
                    chain:mkdir,
                  }, [{dst_dir_path: d.dst_dir_path}])
             )
         })

    })

    Array("move","copy").forEach(cat=>{
        router.post("/"+cat, function(req,res,next){
             return vali.async(req.body.json, {
                src_pathes: {presence:true, isPathArray: {stripTailingSlash:true, stripHeadingSlash:true, noPathCharacterRestrictions: true}},
                dst_dir_path: {presence:true, isPath: {stripTailingSlash:true, stripHeadingSlash:true,noPathCharacterRestrictions: true}},
             })
             .then(d=>{
                 var chain = simpleCloneObject(app.config.get("cmd_"+cat))
                 chain.args = chain.args.concat(d.src_pathes)
                 chain.args = chain.args.concat([ d.dst_dir_path || "." ])

                 app.InsertEvent(req, {e_event_type: cat, e_other: true});

                 return req.sendTask(
                      spawnThrough(req.webhosting)({
                        chain:chain,
                      }, [])
                 )
             })

        })

    })


    return router


    function workingDir(req,res,next) {
        return vali.async(req.body.json, {cwd: {presence:true, isPath: {noPathCharacterRestrictions: true}}})
          .then((cwd)=>{
              req.cwd = cwd.cwd
              next()
          })
    }

    function resolveAndCheckAgainstWebhosting(webhosting, pathCandidate) {
       return fs.realpathAsync(pathCandidate).then(rp=>{
          // console.log("!!", webhosting.extras.path_to_use, rp, rp.substr(0, webhosting.extras.path_to_use.length))
          if(rp.substr(0, webhosting.extras.path_to_use.length) != webhosting.extras.path_to_use) throw new MError("INVALID_PATH")
       }).catch(ex=>{
          console.error("cant resolve", pathCandidate, ex)
          if(ex.code == "ENOENT") throw new MError("PATH_NOT_FOUND", {path: pathCandidate}, ex)
          throw ex
       })

    }

    function toReadOnlyOrReadWrite(req, pm) {
        var chattr = app.config.get("cmd_chattr")
        var spawner = spawnThrough(req.webhosting)
        req.webhosting.run_script_as_uid = "0" // run as root
        req.webhosting.run_script_as_gid = "0" // run as root
        app.InsertEvent(req, {e_event_type: "readonly-readwrite", e_other: true});
        return req.sendTask(
            spawner({chain:chattr}, [{p_or_m: pm}]) // without executeImmediately
        )
    }

    function isReadonly(webhosting) {
        var fp = path.join(webhosting.extras.path_to_use, ".readonly")
        return fs.statAsync(fp)
          .then(x=>{
             if(x.uid != 0) {
                 console.error("Unexpected owner for the readonly file", x)
                 throw new Error("Unexpected owner for the readonly file")
             }
             return Promise.resolve(true)
          })
          .catch(ex=>{
             return Promise.resolve(false)
          })
    }

    function spawnThrough(webhosting) {
        var webhosting_id = webhosting.wh_id
        return function() {
            if(!app.perWebhostingTasks[webhosting_id]) app.perWebhostingTasks[webhosting_id] = {}

            var perWebstoreTasks = app.perWebhostingTasks[webhosting_id];

            if(Object.keys(perWebstoreTasks).length >= app.config.get("max_tasks_per_webhosting"))
            {
               console.error("max tasks per webstore reached:", webhosting_id);
               console.inspect("max tasks per webstore reached:", perWebstoreTasks);
               throw new MError("MAX_TASKS_REACHED")
            }

            if(typeof webhosting.run_script_as_uid == "undefined")
               webhosting.run_script_as_uid = webhosting.wh_id
            if(typeof webhosting.run_script_as_gid == "undefined")
               webhosting.run_script_as_gid = app.config.get("run_script_as_gid")

            if(arguments.length > 1) {
              arguments[1].push(webhosting)
              arguments[1].push(app.config.get("cmd_dependencies"))
              arguments[1].push(app.latestDockerImages)
            }

            return app.commander.spawn.apply(null, arguments)
              .then(h=>{
                  perWebstoreTasks[h.id] = arguments;
                  h.executionPromise
                    .catch(ex=>{
                      console.error("Error during Fileman task operation", arguments, ex)
                    })
                    .then(()=>{
                        console.log("Removing task from perWebstoreTasks", h.id)
                        delete perWebstoreTasks[h.id];
                    })

                  return Promise.resolve(h)
              })
        }
    }


}
