var lib = module.exports = function(app, knex) {

   const cronerLib = require("lib-croner.js")
   var croner = cronerLib(app);

   var wie = {};

   const dotq = require("MonsterDotq");
   

   wie.BackupWh = function(wh, req) {
      var re = {};
      return croner.GetWebstoreEntries(wh)
        .then(x=>{
           re.entries =  x.entries;
           Object.keys(re.entries).forEach(entryId=>{
              var entry = re.entries[entryId];
              delete entry.wh_id;
              delete entry.user_id;
           })

           return re;
        })
   }

   wie.GetAllWebhostingIds = function(){
      return croner.GetAllWebhostings()
   }

   wie.RestoreWh = function(wh, in_data, req) {

      return dotq.linearMap({array: Object.keys(in_data.entries), action: function(cronId){
          var ce = in_data.entries[cronId];
          ce.cron_id = cronId;          
          ce.user_id = wh.wh_user_id;
          ce.wh_id = wh.wh_id;
          return croner.Insert(wh, ce);
      }})
      .then(()=>{
          app.InsertEvent(req, {e_event_type: "restore-cron", e_other: wh.wh_id})

      })

   }



   return wie;

}
