// permissions needed ["EMIT_LOG","INFO_ACCOUNT","INFO_WEBHOSTING"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "fileman"

    var fs = require("MonsterDotq").fs();

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    Array("cmd_dependencies","run_script_as_gid", "cron_store_directory_path","alpine_version").forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
    })

    var alpineVersion = config.get("alpine_version");
    var alpineVersionNoDots = alpineVersion.replace(".", "");

    config.defaults({
      "docker_image_versions_path": "/var/lib/monster/docker/latest-images.config",
      "max_tasks_per_webhosting": 10,
      "templates": {},
      "fetch_max_bytes": 1024*1024, // 1mbyte by default
      "cmd_chattr": getDefaultDockerChain([
        "chattr.pl",
        "[wh_id]",
        "[p_or_m]",
        "[web_path_inside_chroot]",
      ], [
        "--privileged"
      ]),
      "cmd_maildirmake": getDefaultDockerChain([
        "maildirmake.sh",
        "[web_path_inside_chroot][dst_dir_path]",
      ]),
      "cmd_find": getDefaultDockerChain([
        "find.sh",
        "[web_path_inside_chroot][path]",
      ]),
      "cmd_mkdir": getDefaultDockerChain([
        "mkdir.sh",
        "[web_path_inside_chroot][dst_dir_path]",
      ]),
      "cmd_download": getDefaultDockerChain([
        "download.sh",
        "[web_path_inside_chroot][src_file]",
      ]),
      "cmd_upload": getDefaultDockerChain([
        "upload.sh",
        "[web_path_inside_chroot][dst_full_path]",
      ]),
      "cmd_untar": getDefaultDockerChain([
        "untar.sh",
        "[web_path_inside_chroot][dst_dir_path]",
        "[tar_params]",
      ]),
      "cmd_unzip": getDefaultDockerChain([
        "unzip.sh",
        "[web_path_inside_chroot]",
        "[dst_dir_path]",
      ]),
      "cmd_cleanup": getDefaultDockerChain([
        "cleanup.sh",
        "[web_path_inside_chroot]",
      ]),
      "cmd_wipe": getDefaultDockerChain([
        "wipe.sh",
        "[web_path_inside_chroot]",
      ]),
      "cmd_remove": getDefaultDockerChain([
        "remove.sh",
        "[web_path_inside_chroot]",
      ]),
      "cmd_delbackup": getDefaultDockerChain([
        "delbackup.sh",
        "[web_path_inside_chroot]",
        "[delbackup_key_or_supplied]",
      ]),
      "cmd_tar": getDefaultDockerChain([
        "tar.sh",
        "[web_path_inside_chroot][dst_dir_path]",
        "[tar_params]",
      ]),
      "cmd_zip": getDefaultDockerChain([
        "zip.sh",
        "[web_path_inside_chroot][dst_dir_path]",
        "[dst_zip_name]",
      ]),
      "cmd_tgz_all": getDefaultDockerChain([
        "tgz-all.sh",
        "[web_path_inside_chroot]",
      ]),
      "cmd_move": getDefaultDockerChain([
        "move.sh",
        "[web_path_inside_chroot]",
      ]),
      "cmd_copy": getDefaultDockerChain([
        "copy.sh",
        "[web_path_inside_chroot]",
      ]),
      "cmd_template": getDefaultDockerChain([
        "template.sh",
        "[web_path_inside_chroot][dst_dir_path]",
      ], [
        "-v",
        "[template_dir]:/mnt"
      ]),      
    })



    var deps = config.get("cmd_dependencies")
    Array("web_path_inside_chroot").forEach(k=>{
       if(!deps[k]) throw new Error("Mandatory configuration option is missing: cmd_dependencies."+k)
    })


    const docker_image_versions_path = config.get("docker_image_versions_path");

    config.appRestPrefix = "/fileman"

    var app = me.Express(config, moduleOptions);
    app.MError = me.Error

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    app.perWebhostingTasks = {}

    var router = app.PromiseRouter(config.appRestPrefix)

    // external service providers will call this route
    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    const MonsterInfoLib = require("MonsterInfo")
    app.MonsterInfoAccount = MonsterInfoLib.account({config: app.config, accountServerRouter: app.ServersRelayer})
    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)

    router.use("/tasks", app.commander.router)

    require("Wie")(app, router, require("lib-wie.js")(app, app.knex));

    router.use("/tests", require("fileman-tests-router.js")(app))

    router.use("/cron", require("fileman-cron-router.js")(app))

    router.use("/:webhosting", queryWebhosting, require("fileman-webhosting-router.js")(app))

    app.Cleanup = function(){
      const del = require('MonsterDel');
      var patternsToDel = []
      patternsToDel.push(path.join(config.get("cron_store_directory_path"), "*.json"))
      return del(patternsToDel,{"force": true})
    }

    initLatestImages();

    return app



    function queryWebhosting(req,res,next) {
        return app.MonsterInfoWebhosting.GetInfo(req.params.webhosting)
          .then(webhostingInfo=>{
              req.webhosting = webhostingInfo

              var category = req.body.json.path_category || "web"

              var p = req.webhosting.extras[category+"_path"]
              if(!p)
                throw new app.MError("INVALID_PATH_CATEGORY")

              return fs.realpathAsync(p)
               .catch(ex=>{
                  if(ex.code == "ENOENT")
                      throw new app.MError("WEBSTORE_ROOT_DIR_NOT_FOUND", null, ex)
                  throw ex
               })

          })
          .then(rp =>{
              req.webhosting.extras.path_to_use = rp
              next()
          })
    }

    function getDefaultDockerChain(argsMain, argsDocker) {
       var allArgs = [
           "run",
           "--rm",
           "-i",
           "-u",
           "[run_script_as_uid]:[run_script_as_gid]",
           "-v",
           "[extras.path_to_use]:[web_path_inside_chroot]",
       ];
       
       if(argsDocker)
         allArgs = allArgs.concat(argsDocker);
       allArgs.push("alpine-"+alpineVersion+"-fileman:[alpine-"+alpineVersionNoDots+"-fileman]");

       allArgs = allArgs.concat(argsMain)

       return {
         "executable":"docker",
         "args": allArgs 
       }
    }


    function initLatestImages(){
       readLatestImages();
       var m;
       try{
         fs.watch(docker_image_versions_path, (eventType, filename)=>{
            console.log("Event received for", filename, eventType)
            if(m) return;
            m = setTimeout(function(){
               m = null;
               readLatestImages();
            }, 2000);
         });
       } catch(ex){
         console.error("Unable to setup filesystem watcher (retrying later)", ex)
         setTimeout(initLatestImages, 10000);
       }
    }

    function readLatestImages(){
       try {
         var c = fs.readFileSync(docker_image_versions_path, "utf8");
         app.latestDockerImages = JSON.parse(c);
         Object.keys(app.latestDockerImages).forEach(k=>{
             var nk = k.replace(".", "");
             if(nk != k){
                app.latestDockerImages[nk] = app.latestDockerImages[k];
                delete app.latestDockerImages[k];
             }
         })
         console.log("Latest images read from", docker_image_versions_path);

      }
      catch(ex){
          console.log("Unable to read latest image stuff", docker_image_versions_path, ex)
      }
    }

}
